db.createCollection("user")
db.user.insertOne(
   {
    "_class" : "com.jjpa.db.mongodb.document.dma.manager.UserDocument",
    "username" : "admin",
    "password" : "ZG1hQGRtaW5tYW5hZ2Vy",
        "createDate" : new Date(),
        "createBy" : "system",
    "lastUpdateDate" : new Date(),
    "lastUpdateBy" : "system",
    "role" : "admin"
   }
)
db.user.insertOne(
   {
    "_class" : "com.jjpa.db.mongodb.document.dma.manager.UserDocument",
    "username" : "beebuddySystemAdmin",
    "password" : "U3lzdGVtQGRtaW5NYW5hZ2Vy",
        "createDate" : new Date(),
        "createBy" : "system",
    "lastUpdateDate" : new Date(),
    "lastUpdateBy" : "system",
    "role" : "system"
        }
)

db.entity_config.drop()
db.createCollection("entity_config")
db.entity_config.insertOne(
	{
		"_class" : "com.jjpa.db.mongodb.document.dma.manager.EntityDocument",
		"entityName" : "default",
		"remark" : "Default Entity",
		"storyName" : "",
		"role" : "",
		"theme" : "",
		"status" : "Active",
		"publishStatus" : "W",
		"createDate" : new Date(),
		"lastUpdateDate" : new Date()
	}
)

db.dma_entity_config.drop()
db.createCollection("dma_entity_config")
db.dma_entity_config.insertOne(
	{
		"entityName" : "default",
		"home" : "WelcomeDMA",
		"firstPage" : "WelcomeDMA",
		"screenList" : [ 
			{
				"source" : "WelcomeDMA",
				"dynamicApi" : null,
				"dynamicDataBinding" : [],
				"templateType" : "static",
				"uiJson" : "{\"$jason\":{\"head\":{\"title\":\"FirstPageTemplate\",\"actions\":{\"$load\":{\"type\":\"$set\",\"options\":{\"background\":\"#ffffff\"},\"success\":{\"type\":\"$render\"},\"error\":{}},\"$on.back\":{\"options\":{\"allowback\":\"false\"}}},\"templates\":{\"body\":{\"header\":{\"style\":{\"background\":\"{{$get.background}}\"}},\"style\":{\"background\":\"{{$get.background}}\",\"border\":\"none\"},\"sections\":[{\"items\":[{\"type\":\"vertical\",\"style\":{\"align\":\"center\",\"width\":\"100%\",\"height\":\"85%\"},\"components\":[{\"type\":\"label\",\"text\":\"Welcome to DMA\",\"style\":{\"size\":\"30\",\"align\":\"center\",\"color\":\"#555555\"},\"visible\":\"true\"}]}]}]}}}}}",
				"actionList" : [],
				"globalInputList" : [],
				"globalOutputList" : []
			}
		],
		"createDate" : new Date(),
		"lastUpdateDate" : new Date(),
		"createBy" : "system",
		"lastUpdateBy" : "system"
	}
)

db.dma_configuration.drop()
db.createCollection("dma_configuration")
db.dma_configuration.insert([
	{
    "configName" : "versionInfo",
    "data" : {
        
    },
    "nativeJson" : {
        "data" : "{\"$jason\":{\"head\":{\"title\":\"SoftwareUpdateTemplate\",\"description\":\"SoftwareUpdateTemplate\",\"styles\":{},\"actions\":{\"$load\":{\"type\":\"$set\",\"options\":{\"headerColor\":\"#000000\",\"headerBgColor\":\"#ffffff\",\"headerTitle\":\"Software Update\",\"version\":\"{{$env.app.version}}\",\"typeMobile\":\"{{$env.device.os.name}}\",\"lasted_version_ios\":\"2.5.4\",\"lasted_version_android\":\"3.6.4\",\"desc_android\":\"Version 3.6.4 \\n -Fix Header must be single line and split to '...' \\n -Add local image resource [icon_navigate.png,icon_currentlocation.png] \\n\\nVerion 3.6.3 \\n -Change refresh image for DVS \\n\\n Version 3.6.1 \\n -Fix bug dropdown not send value \\n\\n  Version 3.6.0 \\n -Add Feature SignaturePad \\n\\nVersion 3.5.0 \\n - Add able to set visibility of Layout  \\n\\nVersion 3.4.1 \\n - Fix bug loading dialog not close when Click deny permission. \\n\\n Version 3.4.0 \\n - Add feature tap info window of marker pin \\n \\n  Version 3.3.1 \\n - Add feature Set Action to onclick window of Marker pin.  \\n - Add feature Compress file image before upload.  \\n - Fix bug when using map GPS not close.\",\"desc_ios\":\"Version 2.5.4\\n - fix bug check box couponent\\n - add image Current Location to local\\n \\nVersion 2.5.3\\n - fix bug button size with text length\\n \\nVersion 2.5.2\\n - edit icon clear signature\\n \\nVersion 2.5.1\\n - fix bug type visible\\n - fix bug default image component\\n - add feature signature\\n \\nVersion 2.4.0\\n - add feature set visible to layout\\n \\nVersion 2.3.0\\n - add feature auto save photo to gallery\",\"link_download_android\":\"https://dl.dropbox.com/s/iu8e62b8htqx3jt/dma_dev-3.6.4.apk\",\"link_download_ios\":\"itms-services://?action=download-manifest&url=https://dl.dropboxusercontent.com/s/8u5ia9fj8q4p9ov/DMA.plist\",\"icon_android\":\"https://cdn2.iconfinder.com/data/icons/ios-7-style-metro-ui-icons/512/MetroUI_OS_Android.png\",\"icon_ios\":\"https://cdn2.iconfinder.com/data/icons/ios-7-style-metro-ui-icons/512/MetroUI_OS_Apple.png\"},\"success\":{\"type\":\"$set\",\"options\":{\"resultData\":\"{{$jason}}\",\"lastedVersion\":\"{{$get.typeMobile =='ios' ? $get.lasted_version_ios : $get.lasted_version_android}}\",\"desc\":\"{{$get.typeMobile =='ios' ?$get.desc_ios : $get.desc_android}}\",\"link_install\":\"{{$get.typeMobile =='ios' ? $get.link_download_ios : $get.link_download_android}}\",\"icon\":\"{{$get.typeMobile =='ios' ? $get.icon_ios : $get.icon_android}}\"},\"success\":{\"type\":\"$set\",\"options\":{\"currentFlag\":\"{{$get.version >= $get.lastedVersion ? 'true' : 'false'}}\"},\"success\":{\"type\":\"$render\",\"success\":{},\"error\":{}}},\"error\":{}},\"error\":{}},\"installApp\":{\"type\":\"$loading\",\"options\":{\"lock\":\"true\"},\"success\":{\"type\":\"$loading\",\"options\":{\"lock\":\"false\"},\"success\":{\"type\":\"$util.browser\",\"options\":{\"url\":\"{{$get.link_install}}\"},\"success\":{},\"error\":{}}}}},\"templates\":{\"body\":{\"header\":{\"style\":{\"background\":\"{{$get.headerBgColor}}\",\"height\":\"20\",\"color\":\"{{$get.headerColor}}\"},\"title\":\"{{$get.headerTitle}}\"},\"style\":{\"border\":\"none\",\"background\":\"#e2e2e2\"},\"sections\":[{\"items\":[{\"type\":\"vertical\",\"style\":{\"padding\":\"0\",\"spacing\":\"20\"},\"components\":[{\"type\":\"space\",\"style\":{\"background\":\"#e2e2e2\",\"height\":\"10\"}},{\"type\":\"horizontal\",\"style\":{\"padding\":[{\"{{#if $get.currentFlag == 'true'}}\":\"0\"},{\"{{#else}}\":\"10\"}],\"spacing\":\"0\",\"height\":[{\"{{#if $get.currentFlag == 'true'}}\":\"0\"},{\"{{#else}}\":\"50\"}],\"newFlag\":\"true\",\"align\":\"center\",\"background\":\"#ffffff\"},\"components\":[{\"type\":\"label\",\"text\":\"Current version : {{$env.app.version}}\",\"style\":{\"padding\":\"0\",\"size\":\"20\",\"color\":\"#555555\"},\"visible\":[{\"{{#if $get.currentFlag == 'true'}}\":\"false\"},{\"{{#else}}\":\"true\"}]}]},{\"type\":\"vertical\",\"style\":{\"padding\":\"0\",\"spacing\":\"0\",\"background\":\"#ffffff\",\"newFlag\":\"true\"},\"components\":[{\"type\":\"label\",\"text\":\"Software Update\",\"style\":{\"padding\":\"10\",\"size\":\"26\",\"color\":\"#555555\"},\"visible\":[{\"{{#if $get.currentFlag == 'true'}}\":\"false\"},{\"{{#else}}\":\"true\"}]},{\"type\":\"horizontal\",\"style\":{\"padding\":[{\"{{#if $get.currentFlag == 'true'}}\":\"0\"},{\"{{#else}}\":\"10\"}],\"spacing\":[{\"{{#if $get.currentFlag == 'true'}}\":\"0\"},{\"{{#else}}\":\"10\"}]},\"components\":[{\"type\":\"image\",\"url\":\"{{$get.icon}}\",\"style\":{\"width\":\"100\",\"height\":[{\"{{#if $get.currentFlag == 'true'}}\":\"0\"},{\"{{#else}}\":\"100\"}]},\"visible\":[{\"{{#if $get.currentFlag == 'true'}}\":\"false\"},{\"{{#else}}\":\"true\"}]},{\"type\":\"vertical\",\"style\":{\"padding\":[{\"{{#if $get.currentFlag == 'true'}}\":\"0\"},{\"{{#else}}\":\"10\"}],\"spacing\":\"0\"},\"components\":[{\"type\":\"label\",\"text\":\"{{$env.device.os.name}} {{$get.lastedVersion}}\",\"style\":{\"padding\":\"0\",\"size\":\"18\",\"color\":\"#555555\"},\"visible\":[{\"{{#if $get.currentFlag == 'true'}}\":\"false\"},{\"{{#else}}\":\"true\"}]}]}]},{\"type\":\"vertical\",\"style\":{\"padding_left\":[{\"{{#if $get.currentFlag == 'true'}}\":\"0\"},{\"{{#else}}\":\"20\"}],\"padding_right\":[{\"{{#if $get.currentFlag == 'true'}}\":\"0\"},{\"{{#else}}\":\"20\"}],\"padding_bottom\":[{\"{{#if $get.currentFlag == 'true'}}\":\"0\"},{\"{{#else}}\":\"20\"}],\"spacing\":\"0\"},\"components\":[{\"type\":\"label\",\"text\":\"{{$get.desc}}\",\"style\":{\"size\":\"18\",\"color\":\"#555555\"},\"visible\":[{\"{{#if $get.currentFlag == 'true'}}\":\"false\"},{\"{{#else}}\":\"true\"}]}]}]},{\"type\":\"vertical\",\"style\":{\"padding_left\":\"20\",\"spacing\":\"0\",\"background\":\"#ffffff\"},\"components\":[{\"type\":\"button\",\"text\":\"Install Now\",\"style\":{\"width\":\"100%\",\"height\":[{\"{{#if $get.currentFlag == 'true'}}\":\"0\"},{\"{{#else}}\":\"50\"}],\"size\":\"18\",\"align\":\"left\",\"color\":\"#4993d1\"},\"visible\":[{\"{{#if $get.currentFlag == 'true'}}\":\"false\"},{\"{{#else}}\":\"true\"}],\"action\":{\"trigger\":\"installApp\"}}]},{\"type\":\"space\",\"style\":{\"height\":[{\"{{#if $get.currentFlag == 'true'}}\":\"20%\"},{\"{{#else}}\":\"0\"}]}},{\"type\":\"vertical\",\"style\":{\"padding\":\"0\",\"spacing\":\"0\",\"align\":\"center\"},\"components\":[{\"type\":\"label\",\"text\":\"{{$env.device.os.name}} {{$env.app.version}}\",\"style\":{\"padding\":\"0\",\"size\":\"20\",\"color\":\"#555555\"},\"visible\":\"{{$get.currentFlag}}\"},{\"type\":\"label\",\"text\":\"Your app is up to date.\",\"style\":{\"padding\":\"0\",\"size\":\"20\",\"color\":\"#555555\"},\"visible\":\"{{$get.currentFlag}}\"}]}]}]}]}}}}}"
    },
    "lastUpdateBy" : "systemadmin"
},{
    "configName" : "forceLogout",
    "data" : {
        
    },
    "nativeJson" : {
        "data" : "{\"$jason\":{\"head\":{\"title\":\"Force Logout\",\"actions\":{\"$load\":{\"type\":\"$set\",\"options\":{\"_dma_param_internal_first_page\":null,\"_dma_param_internal_publish_version\":\"5.74\",\"_dma_param_internal_application\":\"default\",\"_dma_param_internal_domain\":\"https://dma.beebuddy.net\",\"title\":\"Application\"},\"success\":{\"type\":\"$render\",\"success\":{\"type\":\"$global.resetAll\",\"success\":{\"type\":\"$session.resetAll\",\"success\":{\"type\":\"$passcode.reset\",\"success\":{\"type\":\"$once.resetAll\",\"success\":{\"type\":\"$util.bannerAlert\",\"options\":{\"title\":\"Notification\",\"description\":\"Application has been updated.\",\"type\":\"info\"},\"success\":{\"type\":\"$util.back\",\"options\":{\"allowback\":\"true\",\"url\":\"{{$get._dma_param_internal_domain || ''}}/rest/ws/dma/loadConfig?contentType=200&source=2&message=OpenPage&_dma_entity={{$get._dma_param_internal_application || ''}}&_dma_type=first\"}},\"error\":{}},\"error\":{\"type\":\"$util.alert\",\"options\":{\"title\":\"Reset once\",\"description\":\"error\"},\"success\":{},\"error\":{}}},\"error\":{\"type\":\"$util.alert\",\"options\":{\"title\":\"Reset passcode\",\"description\":\"error\"},\"success\":{},\"error\":{}}},\"error\":{\"type\":\"$util.alert\",\"options\":{\"title\":\"Reset session\",\"description\":\"error\"},\"success\":{},\"error\":{}}},\"error\":{\"type\":\"$util.alert\",\"options\":{\"title\":\"Reset global\",\"description\":\"error\"},\"success\":{},\"error\":{}}},\"error\":{}},\"error\":{}},\"$on.back\":{\"options\":{\"allowback\":\"true\",\"url\":\"{{$get._dma_param_internal_domain || ''}}/rest/ws/dma/loadConfig?contentType=200&source=2&message=OpenPage&_dma_entity={{$get._dma_param_internal_application || ''}}&_dma_type=first\"}}},\"templates\":{\"body\":{\"header\":{\"style\":{\"background\":\"#808285\",\"height\":\"20\",\"color\":\"#ffffff\"},\"title\":\"{{$get.title}}\"},\"style\":{\"border\":\"none\"},\"sections\":[{\"header\":{\"type\":\"vertical\",\"style\":{\"padding\":\"10\",\"spacing\":\"0\",\"align\":\"center\"},\"components\":[{\"type\":\"space\",\"style\":{\"height\":\"40%\"}},{\"type\":\"label\",\"text\":\"Application has been updated.\",\"style\":{\"size\":\"20\",\"color\":\"#a1a1a1\",\"padding\":\"0\",\"corner_radius\":\"0\"},\"visible\":\"true\"}]}}]}}}}}"
    },
    "lastUpdateBy" : "systemadmin"
},{
    "configName" : "previewTemplate",
    "data" : {
        
    },
    "nativeJson" : {
        "data" : "{\"$jason\":{\"head\":{\"title\":\"PreviewTemplate\",\"actions\":{\"$load\":{\"type\":\"$set\",\"options\":{\"_internal_domain_url\":\"https://dma.beebuddy.net\",\"background\":\"#ffde16\",\"color\":\"#209dd4\",\"version\":\"{{$env.app.version}}\"},\"success\":{\"type\":\"$render\"}},\"click_view\":{\"type\":\"$href\",\"options\":{\"url\":\"{{$get._internal_domain_url}}/rest/ws/dma/loadConfig?contentType=200&source=2&message=OpenPage&_dma_preview=s\",\"options\":{\"data\":{\"contentType\":\"200\",\"source\":\"2\",\"message\":\"Open page\",\"_dma_data\":\"\",\"_dma_preview\":\"ssdsd\",\"previewNumber\":\"{{$get.previewNumber}}\"}}},\"success\":{},\"error\":{}}},\"templates\":{\"body\":{\"header\":{\"style\":{\"background\":\"{{$get.background}}\",\"height\":\"20\",\"color\":\"{{$get.color}}\"},\"title\":\"\"},\"style\":{\"border\":\"none\",\"background\":\"{{$get.background}}\"},\"sections\":[{\"items\":[{\"type\":\"vertical\",\"style\":{\"padding\":\"10\",\"spacing\":\"0\",\"align\":\"center\"},\"components\":[{\"type\":\"image\",\"url\":\"https://sale.beebuddy.net/ims/apis/file/Download?key=VkxoWEw2Sy90eXlsR0JtVnpSaHc5VTBPbXEzanZGd2xMUmJvTlVibW0rOTdHMDlDWFJyengvUTRpOHY1RnV5aVFvUkFGM1N2NE03OUtpYkxSL2E1cXlhcFBqOHEyam15NE9iVTJsdmp5VnlPbCtJZ2d6UTJxckRveHNhZWFJT0dZWXJ2d0xwTVpHc1l2S2oyVXNPRjVkVHZFVExyUGhpNGVKKzZCbDcvQ253UkR3aTI1KzZ4TmdoRFF2YXU3YlJDMVVLVjFoKzl0aTEvcTN4UGRaNjd0MUxWazBXQzVIc21BRFZETWMwQlEzeUpNRlc3WXVXd09RPT0=.png\",\"header\":{},\"style\":{\"width\":\"120\",\"height\":\"120\",\"corner_radius\":\"0\"},\"visible\":\"true\"},{\"type\":\"space\",\"style\":{\"height\":\"10\"}},{\"type\":\"label\",\"text\":\"Please Enter Code\",\"style\":{\"size\":\"28\",\"color\":\"{{$get.color}}\",\"padding\":\"0\",\"corner_radius\":\"0\"},\"visible\":\"true\"},{\"type\":\"space\",\"style\":{\"height\":\"10\"}},{\"type\":\"textfield\",\"name\":\"previewNumber\",\"placeholder\":\"####\",\"keyboard\":\"phone\",\"style\":{\"background\":\"{{$get.background}}\",\"align\":\"center\",\"size\":\"22\",\"color\":\"{{$get.color}}\",\"placeholder_color\":\"#c4c4c4\",\"border_color\":\"{{$get.background}}\",\"padding_left\":\"5\",\"width\":\"200\",\"height\":\"40\",\"corner_radius\":\"10\",\"secure\":\"false\"},\"visible\":\"true\",\"enable\":\"true\"},{\"type\":\"space\",\"style\":{\"background\":\"{{$get.color}}\",\"height\":\"1\",\"width\":\"200\"}},{\"type\":\"space\",\"style\":{\"height\":\"10\"}},{\"type\":\"label\",\"text\":\"App Version {{$get.version}}\",\"style\":{\"size\":\"16\",\"color\":\"{{$get.color}}\"},\"visible\":\"true\"},{\"type\":\"space\",\"style\":{\"height\":\"30\"}},{\"type\":\"button\",\"text\":\"View\",\"style\":{\"width\":\"220\",\"height\":\"45\",\"color\":\"#ffffff\",\"size\":\"20\",\"background\":\"{{$get.color}}\",\"corner_radius\":\"23\"},\"visible\":\"true\",\"enable\":\"true\",\"action\":{\"trigger\":\"click_view\"}}]}]}]}}}}}"
    },
    "lastUpdateBy" : "systemadmin"
}]
)

db.storyboard_target_type.drop()
db.createCollection("storyboard_target_type")
db.storyboard_target_type.insert(
	[
		{
			"name" : "Extra",
			"api" : "listExtraTarget"
		},
		{
			"name" : "Screen",
			"api" : "listAppUi"
		}
	]
)

db.storyboard_extra_target_type.drop()
db.createCollection("storyboard_extra_target_type")
db.storyboard_extra_target_type.insert(
	[
		{
			"uiName" : "self"
		},
		{
			"uiName" : "refresh"
		},
		{
			"uiName" : "browser",
			"configParameter" : [ 
				{
					"option" : "url",
					"desc" : "ที่อยู่ของเว็บไซต์",
					"defaultValue" : "https://www.google.com",
					"value" : "",
					"dmaType" : "open_browser"
				}, 
				{
					"option" : "extra",
					"desc" : "เพิ่มเติม",
					"defaultValue" : "รายละเอียดเพิ่มเติม",
					"value" : "",
					"dmaType" : "open_browser"
				}
			]
		},{
			"uiName" : "dial",
			"configParameter" : [ 
				{
					"option" : "tel",
					"desc" : "เบอร์โทรศัพท์",
					"defaultValue" : "0123456789",
					"value" : "",
					"dmaType" : "phone_call"
				}
			]
		},{
			"uiName" : "paging",
			"configParameter" : [ 
				{
					"option" : "_tp_currentPage",
					"desc" : "Current Page",
					"defaultValue" : "",
					"value" : "",
					"dmaType" : "set_paging"
				}, 
				{
					"option" : "_tp_totalPage",
					"desc" : "Total Page",
					"defaultValue" : "",
					"value" : "",
					"dmaType" : "set_paging"
				}, 
				{
					"option" : "_tp_sizePerPage",
					"desc" : "Size Per Page",
					"defaultValue" : "",
					"value" : "",
					"dmaType" : "set_paging"
				}, 
				{
					"option" : "_tp_offset",
					"desc" : "offset",
					"defaultValue" : "",
					"value" : "",
					"dmaType" : "set_paging"
				}
			]
		},{
			"uiName" : "bannerSuccess",
			"configParameter" : [ 
				{
					"option" : "title",
					"desc" : "Title",
					"defaultValue" : "Title",
					"value" : "",
					"dmaType" : "banner_alert_success"
				}, 
				{
					"option" : "description",
					"desc" : "description",
					"defaultValue" : "description",
					"value" : "",
					"dmaType" : "banner_alert_success"
				}, 
				{
					"option" : "type",
					"desc" : "success แสดงสีเขียว , error แสดงสีส้ม , info แสดงสีน้ำเงิน",
					"defaultValue" : "success",
					"value" : "",
					"dmaType" : "banner_alert_success"
				}
			]
		},{
			"uiName" : "preview",
			"configParameter" : [ 
				{
					"option" : "url",
					"desc" : "Image url",
					"defaultValue" : "",
					"value" : "",
					"dmaType" : "preview_image"
				}
			]
		}
	]
)

db.screen_input_type.drop()
db.createCollection("screen_input_type")
db.screen_input_type.insert(
	[
		{
			"name" : "Global",
			"api" : "listGlobalValue"
		},
		{
			"name" : "Native",
			"api" : "listScreenNativeInputType"
		}
	]
)

db.screen_native_input_type.drop()
db.createCollection("screen_native_input_type")
db.screen_native_input_type.insert(
	[
		{
			"globalName" : "Not required location permission",
			"fieldType" : "Location",
			"source" : "Native",
			"api" : "",
			"action" : "",
			"dataStructure" : "",
			"className" : "LocationBlank",
			"dmaParameter" : "_dma_internal_location"
		},
		{
			"globalName" : "Required location permission",
			"fieldType" : "Location",
			"source" : "Native",
			"api" : "",
			"action" : "",
			"dataStructure" : "",
			"className" : "LocationCheck",
			"dmaParameter" : "_dma_internal_location"
		},
		{
			"globalName" : "Not required location permission with alert",
			"fieldType" : "Location",
			"source" : "Native",
			"api" : "",
			"action" : "",
			"dataStructure" : "",
			"className" : "LocationBlankAlert",
			"dmaParameter" : "_dma_internal_location"
		}
	]
)

db.dma_action_chain_template.drop()
db.createCollection("dma_action_chain_template")
db.dma_action_chain_template.insert([{
		"actionType": "detailList_confirm_api_change",
		"chainTemplate": {
			"type": "$set",
			"dmaType": "prepare_list",
			"options": {
				"selected_details_item": "{{$jason.my}}"
			},
			"success": {
				"type": "$util.alert",
				"dmaType": "confirm_dialog",
				"options": {
					"title": "Hello",
					"description": "This is a message",
					"positiveText": "Yes",
					"negetiveText": "No"
				},
				"success": {
					"type": "$loading",
					"dmaType": "open_loading",
					"options": {
						"lock": "true"
					},
					"success": {
						"type": "$set",
						"dmaType": "set_global_to_api",
						"options": {},
						"success": {
							"type": "$network.request",
							"dmaType": "network_request",
							"options": {
								"url": "http://www.dma.beebuddy.net/myfile.json",
								"method": "post",
								"header": {
									"content_type": "json"
								},
								"data": {}
							},
							"success": {
								"type": "$set",
								"dmaType": "response_to_local",
								"options": {
									"result": "{{$jason}}"
								},
								"success": {
									"type": "$loading",
									"dmaType": "hide_loading",
									"options": {
										"lock": "false"
									},
									"success": {
										"type": "$util.condition",
										"dmaType": "check_condition",
										"options": {
											"expression": "true"
										},
										"success": {
											"type": "$global.set",
											"dmaType": "set_detail_list_global",
											"options": {
												"output1": "{{$get.result.resultData}}"
											},
											"success": {
												"type": "$href",
												"dmaType": "change_page",
												"options": {
													"url": "www.mylinkurl.com/myfile.json"
												},
												"success": {},
												"error": {}
											},
											"error": {}
										},
										"error": {
											"type": "$util.banner",
											"dmaType": "condition_Banner",
											"options": {
												"title": "Hello",
												"description": "{{$get.result.resultMessage}}",
												"type": "error"
											},
											"success": {},
											"error": {}
										}
									}
								}
							},
							"error": {
								"type": "$loading",
								"dmaType": "hide_loading",
								"options": {
									"lock": "false"
								},
								"success": {
									"type": "$util.banner",
									"dmaType": "network_banner",
									"options": {
										"title": "Hello",
										"description": "Can not connect to server.",
										"type": "error"
									},
									"success": {},
									"error": {
										"type": "$util.alert",
										"dmaType": "network_banner",
										"options": {
											"title": "Hello",
											"description": "Do you want to exit?",
											"positiveText": "Yes",
											"negetiveText": "No",
											"form": []
										},
										"success": {},
										"error": {}
									}
								}
							}
						},
						"error": {}
					}
				},
				"error": {}
			}
		},
		"refference": {
			"version": "3.0.2",
			"required": {
				"dmaActionChainRequired": [{
						"name": "prepare_list",
						"version": "3.0.2"
					}, {
						"name": "confirm_dialog",
						"version": "3.0.2"
					}, {
						"name": "open_loading",
						"version": "3.0.2"
					}, {
						"name": "set_global_to_api",
						"version": "3.0.2"
					}, {
						"name": "network_request",
						"version": "3.0.2"
					}, {
						"name": "response_to_local",
						"version": "3.0.2"
					}, {
						"name": "hide_loading",
						"version": "3.0.2"
					}, {
						"name": "check_condition",
						"version": "3.0.2"
					}, {
						"name": "set_detail_list_global",
						"version": "3.0.2"
					}, {
						"name": "change_page",
						"version": "3.0.2"
					}, {
						"name": "condition_banner",
						"version": "3.0.2"
					}, {
						"name": "network_banner",
						"version": "3.0.2"
					}
				]
			}
		},
		"version": "3.0.2",
		"lastUpdateDate": new Date(),
		"lastUpdateBy": "beebuddySystemAdmin",
		"createDate": new Date()
	}, {
		"actionType": "detailList_confirm_api_self",
		"chainTemplate": {
			"type": "$set",
			"dmaType": "prepare_list",
			"options": {
				"selected_details_item": "{{$jason.my}}"
			},
			"success": {
				"type": "$util.alert",
				"dmaType": "confirm_dialog",
				"options": {
					"title": "Hello",
					"description": "This is a message",
					"positiveText": "Yes",
					"negetiveText": "No"
				},
				"success": {
					"type": "$loading",
					"dmaType": "open_loading",
					"options": {
						"lock": "true"
					},
					"success": {
						"type": "$set",
						"dmaType": "set_global_to_api",
						"options": {},
						"success": {
							"type": "$network.request",
							"dmaType": "network_request",
							"options": {
								"url": "http://www.dma.beebuddy.net/myfile.json",
								"method": "post",
								"header": {
									"content_type": "json"
								},
								"data": {}
							},
							"success": {
								"type": "$set",
								"dmaType": "response_to_local",
								"options": {
									"result": "{{$jason}}"
								},
								"success": {
									"type": "$loading",
									"dmaType": "hide_loading",
									"options": {
										"lock": "false"
									},
									"success": {
										"type": "$util.condition",
										"dmaType": "check_condition",
										"options": {
											"expression": "true"
										},
										"success": {
											"type": "$global.set",
											"dmaType": "set_detail_list_global",
											"options": {
												"output1": "{{$get.result.resultData}}"
											},
											"success": {},
											"error": {}
										},
										"error": {
											"type": "$util.banner",
											"dmaType": "condition_Banner",
											"options": {
												"title": "Hello",
												"description": "{{$get.result.resultMessage}}",
												"type": "error"
											},
											"success": {},
											"error": {}
										}
									}
								}
							},
							"error": {
								"type": "$loading",
								"dmaType": "hide_loading",
								"options": {
									"lock": "false"
								},
								"success": {
									"type": "$util.banner",
									"dmaType": "network_banner",
									"options": {
										"title": "Hello",
										"description": "Can not connect to server.",
										"type": "error"
									},
									"success": {},
									"error": {
										"type": "$util.alert",
										"dmaType": "network_banner",
										"options": {
											"title": "Hello",
											"description": "Do you want to exit?",
											"positiveText": "Yes",
											"negetiveText": "No",
											"form": []
										},
										"success": {},
										"error": {}
									}
								}
							}
						},
						"error": {}
					}
				},
				"error": {}
			}
		},
		"refference": {
			"version": "3.0.2",
			"required": {
				"dmaActionChainRequired": [{
						"name": "prepare_list",
						"version": "3.0.2"
					}, {
						"name": "confirm_dialog",
						"version": "3.0.2"
					}, {
						"name": "open_loading",
						"version": "3.0.2"
					}, {
						"name": "set_global_to_api",
						"version": "3.0.2"
					}, {
						"name": "network_request",
						"version": "3.0.2"
					}, {
						"name": "response_to_local",
						"version": "3.0.2"
					}, {
						"name": "hide_loading",
						"version": "3.0.2"
					}, {
						"name": "check_condition",
						"version": "3.0.2"
					}, {
						"name": "set_detail_list_global",
						"version": "3.0.2"
					}, {
						"name": "condition_banner",
						"version": "3.0.2"
					}, {
						"name": "network_banner",
						"version": "3.0.2"
					}
				]
			}
		},
		"version": "3.0.2",
		"lastUpdateDate": new Date(),
		"lastUpdateBy": "beebuddySystemAdmin",
		"createDate": new Date()
	}, {
		"actionType": "detailList_confirm_noapi_change",
		"chainTemplate": {
			"type": "$set",
			"dmaType": "prepare_list",
			"options": {
				"selected_details_item": "{{$jason.my}}"
			},
			"success": {
				"type": "$util.alert",
				"dmaType": "confirm_dialog",
				"options": {
					"title": "Hello",
					"description": "This is a message",
					"positiveText": "Yes",
					"negetiveText": "No"
				},
				"success": {
					"type": "$global.set",
					"dmaType": "set_detail_list_global",
					"options": {
						"output1": "{{$get.result.resultData}}"
					},
					"success": {
						"type": "$href",
						"dmaType": "change_page",
						"options": {
							"url": "www.mylinkurl.com/myfile.json"
						},
						"success": {},
						"error": {}
					},
					"error": {}
				},
				"error": {}
			}
		},
		"refference": {
			"version": "3.0.2",
			"required": {
				"dmaActionChainRequired": [{
						"name": "prepare_list",
						"version": "3.0.2"
					}, {
						"name": "confirm_dialog",
						"version": "3.0.2"
					}, {
						"name": "set_detail_list_global",
						"version": "3.0.2"
					}, {
						"name": "change_page",
						"version": "3.0.2"
					}
				]
			}
		},
		"version": "3.0.2",
		"lastUpdateDate": new Date(),
		"lastUpdateBy": "beebuddySystemAdmin",
		"createDate": new Date()
	}, {
		"actionType": "detailList_confirm_noapi_self",
		"chainTemplate": {
			"type": "$set",
			"dmaType": "prepare_list",
			"options": {
				"selected_details_item": "{{$jason.my}}"
			},
			"success": {
				"type": "$util.alert",
				"dmaType": "confirm_dialog",
				"options": {
					"title": "Hello",
					"description": "This is a message",
					"positiveText": "Yes",
					"negetiveText": "No"
				},
				"success": {
					"type": "$global.set",
					"dmaType": "set_detail_list_global",
					"options": {
						"output1": "{{$get.result.resultData}}"
					},
					"success": {},
					"error": {}
				},
				"error": {}
			}
		},
		"refference": {
			"version": "3.0.2",
			"required": {
				"dmaActionChainRequired": [{
						"name": "prepare_list",
						"version": "3.0.2"
					}, {
						"name": "confirm_dialog",
						"version": "3.0.2"
					}, {
						"name": "set_detail_list_global",
						"version": "3.0.2"
					}
				]
			}
		},
		"version": "3.0.2",
		"lastUpdateDate": new Date(),
		"lastUpdateBy": "beebuddySystemAdmin",
		"createDate": new Date()
	}, {
		"actionType": "detailList_noconfirm_api_change",
		"chainTemplate": {
			"type": "$set",
			"dmaType": "prepare_list",
			"options": {
				"selected_details_item": "{{$jason.my}}"
			},
			"success": {
				"type": "$loading",
				"dmaType": "open_loading",
				"options": {
					"lock": "true"
				},
				"success": {
					"type": "$set",
					"dmaType": "set_global_to_api",
					"options": {},
					"success": {
						"type": "$network.request",
						"dmaType": "network_request",
						"options": {
							"url": "http://www.dma.beebuddy.net/myfile.json",
							"method": "post",
							"header": {
								"content_type": "json"
							},
							"data": {}
						},
						"success": {
							"type": "$set",
							"dmaType": "response_to_local",
							"options": {
								"result": "{{$jason}}"
							},
							"success": {
								"type": "$loading",
								"dmaType": "hide_loading",
								"options": {
									"lock": "false"
								},
								"success": {
									"type": "$util.condition",
									"dmaType": "check_condition",
									"options": {
										"expression": "true"
									},
									"success": {
										"type": "$global.set",
										"dmaType": "set_detail_list_global",
										"options": {
											"output1": "{{$get.result.resultData}}"
										},
										"success": {
											"type": "$href",
											"dmaType": "change_page",
											"options": {
												"url": "www.mylinkurl.com/myfile.json"
											},
											"success": {},
											"error": {}
										},
										"error": {}
									},
									"error": {
										"type": "$util.banner",
										"dmaType": "condition_Banner",
										"options": {
											"title": "Hello",
											"description": "{{$get.result.resultMessage}}",
											"type": "error"
										},
										"success": {},
										"error": {}
									}
								}
							}
						},
						"error": {
							"type": "$loading",
							"dmaType": "hide_loading",
							"options": {
								"lock": "false"
							},
							"success": {
								"type": "$util.banner",
								"dmaType": "network_banner",
								"options": {
									"title": "Hello",
									"description": "Can not connect to server.",
									"type": "error"
								},
								"success": {},
								"error": {
									"type": "$util.alert",
									"dmaType": "network_banner",
									"options": {
										"title": "Hello",
										"description": "Do you want to exit?",
										"positiveText": "Yes",
										"negetiveText": "No",
										"form": []
									},
									"success": {},
									"error": {}
								}
							}
						}
					},
					"error": {}
				}
			}
		},
		"refference": {
			"version": "3.0.2",
			"required": {
				"dmaActionChainRequired": [{
						"name": "prepare_list",
						"version": "3.0.2"
					}, {
						"name": "open_loading",
						"version": "3.0.2"
					}, {
						"name": "set_global_to_api",
						"version": "3.0.2"
					}, {
						"name": "network_request",
						"version": "3.0.2"
					}, {
						"name": "response_to_local",
						"version": "3.0.2"
					}, {
						"name": "hide_loading",
						"version": "3.0.2"
					}, {
						"name": "check_condition",
						"version": "3.0.2"
					}, {
						"name": "set_detail_list_global",
						"version": "3.0.2"
					}, {
						"name": "change_page",
						"version": "3.0.2"
					}, {
						"name": "condition_banner",
						"version": "3.0.2"
					}, {
						"name": "network_banner",
						"version": "3.0.2"
					}
				]
			}
		},
		"version": "3.0.2",
		"lastUpdateDate": new Date(),
		"lastUpdateBy": "beebuddySystemAdmin",
		"createDate": new Date()
	}, {
		"actionType": "detailList_noconfirm_api_self",
		"chainTemplate": {
			"type": "$set",
			"dmaType": "prepare_list",
			"options": {
				"selected_details_item": "{{$jason.my}}"
			},
			"success": {
				"type": "$loading",
				"dmaType": "open_loading",
				"options": {
					"lock": "true"
				},
				"success": {
					"type": "$set",
					"dmaType": "set_global_to_api",
					"options": {},
					"success": {
						"type": "$network.request",
						"dmaType": "network_request",
						"options": {
							"url": "http://www.dma.beebuddy.net/myfile.json",
							"method": "post",
							"header": {
								"content_type": "json"
							},
							"data": {}
						},
						"success": {
							"type": "$set",
							"dmaType": "response_to_local",
							"options": {
								"result": "{{$jason}}"
							},
							"success": {
								"type": "$loading",
								"dmaType": "hide_loading",
								"options": {
									"lock": "false"
								},
								"success": {
									"type": "$util.condition",
									"dmaType": "check_condition",
									"options": {
										"expression": "true"
									},
									"success": {
										"type": "$global.set",
										"dmaType": "set_detail_list_global",
										"options": {
											"output1": "{{$get.result.resultData}}"
										},
										"success": {},
										"error": {}
									},
									"error": {
										"type": "$util.banner",
										"dmaType": "condition_Banner",
										"options": {
											"title": "Hello",
											"description": "{{$get.result.resultMessage}}",
											"type": "error"
										},
										"success": {},
										"error": {}
									}
								}
							}
						},
						"error": {
							"type": "$loading",
							"dmaType": "hide_loading",
							"options": {
								"lock": "false"
							},
							"success": {
								"type": "$util.banner",
								"dmaType": "network_banner",
								"options": {
									"title": "Hello",
									"description": "Can not connect to server.",
									"type": "error"
								},
								"success": {},
								"error": {
									"type": "$util.alert",
									"dmaType": "network_banner",
									"options": {
										"title": "Hello",
										"description": "Do you want to exit?",
										"positiveText": "Yes",
										"negetiveText": "No",
										"form": []
									},
									"success": {},
									"error": {}
								}
							}
						}
					},
					"error": {}
				}
			}
		},
		"refference": {
			"version": "3.0.2",
			"required": {
				"dmaActionChainRequired": [{
						"name": "prepare_list",
						"version": "3.0.2"
					}, {
						"name": "open_loading",
						"version": "3.0.2"
					}, {
						"name": "set_global_to_api",
						"version": "3.0.2"
					}, {
						"name": "network_request",
						"version": "3.0.2"
					}, {
						"name": "response_to_local",
						"version": "3.0.2"
					}, {
						"name": "hide_loading",
						"version": "3.0.2"
					}, {
						"name": "check_condition",
						"version": "3.0.2"
					}, {
						"name": "set_detail_list_global",
						"version": "3.0.2"
					}, {
						"name": "condition_banner",
						"version": "3.0.2"
					}, {
						"name": "network_banner",
						"version": "3.0.2"
					}
				]
			}
		},
		"version": "3.0.2",
		"lastUpdateDate": new Date(),
		"lastUpdateBy": "beebuddySystemAdmin",
		"createDate": new Date()
	}, {
		"actionType": "detailList_noconfirm_noapi_change",
		"chainTemplate": {
			"type": "$set",
			"dmaType": "prepare_list",
			"options": {
				"selected_details_item": "{{$jason.my}}"
			},
			"success": {
				"type": "$global.set",
				"dmaType": "set_detail_list_global",
				"options": {
					"output1": "{{$get.result.resultData}}"
				},
				"success": {
					"type": "$href",
					"dmaType": "change_page",
					"options": {
						"url": "www.mylinkurl.com/myfile.json"
					},
					"success": {},
					"error": {}
				},
				"error": {}
			}
		},
		"refference": {
			"version": "3.0.2",
			"required": {
				"dmaActionChainRequired": [{
						"name": "prepare_list",
						"version": "3.0.2"
					}, {
						"name": "set_detail_list_global",
						"version": "3.0.2"
					}, {
						"name": "change_page",
						"version": "3.0.2"
					}
				]
			}
		},
		"version": "3.0.2",
		"lastUpdateDate": new Date(),
		"lastUpdateBy": "beebuddySystemAdmin",
		"createDate": new Date()
	}, {
		"actionType": "detailList_noconfirm_noapi_self",
		"chainTemplate": {
			"type": "$set",
			"dmaType": "prepare_list",
			"options": {
				"selected_details_item": "{{$jason.my}}"
			},
			"success": {
				"type": "$global.set",
				"dmaType": "set_detail_list_global",
				"options": {
					"output1": "{{$get.result.resultData}}"
				},
				"success": {},
				"error": {}
			}
		},
		"refference": {
			"version": "3.0.2",
			"required": {
				"dmaActionChainRequired": [{
						"name": "prepare_list",
						"version": "3.0.2"
					}, {
						"name": "set_detail_list_global",
						"version": "3.0.2"
					}
				]
			}
		},
		"version": "3.0.2",
		"lastUpdateDate": new Date(),
		"lastUpdateBy": "\"admin\"",
		"createDate": new Date()
	}, {
		"actionType": "login_confirm_api_change",
		"chainTemplate": {
			"type": "$util.alert",
			"dmaType": "confirm_dialog",
			"options": {
				"title": "Hello",
				"description": "This is a message",
				"positiveText": "Yes",
				"negetiveText": "No"
			},
			"success": {
				"type": "$loading",
				"dmaType": "open_loading",
				"options": {
					"lock": "true"
				},
				"success": {
					"type": "$set",
					"dmaType": "set_global_to_api",
					"options": {},
					"success": {
						"type": "$network.request",
						"dmaType": "network_request",
						"options": {
							"url": "http://www.dma.beebuddy.net/myfile.json",
							"method": "post",
							"header": {
								"content_type": "json"
							},
							"data": {}
						},
						"success": {
							"type": "$set",
							"dmaType": "response_to_local",
							"options": {
								"result": "{{$jason}}"
							},
							"success": {
								"type": "$loading",
								"dmaType": "hide_loading",
								"options": {
									"lock": "false"
								},
								"success": {
									"type": "$util.condition",
									"dmaType": "check_condition",
									"options": {
										"expression": "true"
									},
									"success": {
										"type": "$global.set",
										"dmaType": "set_global",
										"options": {
											"output1": "{{$get.result.resultData}}"
										},
										"success": {
											"type": "$session.set",
											"dmaType": "set_session",
											"options": {
												"domain": "jasonbase.com",
												"header": {},
												"body": {}
											},
											"success": {
												"type": "$href",
												"dmaType": "change_page",
												"options": {
													"url": "www.mylinkurl.com/myfile.json"
												},
												"success": {},
												"error": {}
											},
											"error": {}
										},
										"error": {}
									},
									"error": {
										"type": "$util.banner",
										"dmaType": "condition_Banner",
										"options": {
											"title": "Hello",
											"description": "{{$get.result.resultMessage}}",
											"type": "error"
										},
										"success": {},
										"error": {}
									}
								}
							}
						},
						"error": {
							"type": "$loading",
							"dmaType": "hide_loading",
							"options": {
								"lock": "false"
							},
							"success": {
								"type": "$util.banner",
								"dmaType": "network_banner",
								"options": {
									"title": "Hello",
									"description": "Can not connect to server.",
									"type": "error"
								},
								"success": {},
								"error": {
									"type": "$util.alert",
									"dmaType": "network_banner",
									"options": {
										"title": "Hello",
										"description": "Do you want to exit?",
										"positiveText": "Yes",
										"negetiveText": "No",
										"form": []
									},
									"success": {},
									"error": {}
								}
							}
						}
					},
					"error": {}
				}
			},
			"error": {}
		},
		"refference": {
			"version": "3.0.2",
			"required": {
				"dmaActionChainRequired": [{
						"name": "confirm_dialog",
						"version": "3.0.2"
					}, {
						"name": "open_loading",
						"version": "3.0.2"
					}, {
						"name": "set_global_to_api",
						"version": "3.0.2"
					}, {
						"name": "network_request",
						"version": "3.0.2"
					}, {
						"name": "response_to_local",
						"version": "3.0.2"
					}, {
						"name": "hide_loading",
						"version": "3.0.2"
					}, {
						"name": "check_condition",
						"version": "3.0.2"
					}, {
						"name": "set_global",
						"version": "3.0.2"
					}, {
						"name": "set_session",
						"version": "3.0.2"
					}, {
						"name": "change_page",
						"version": "3.0.2"
					}, {
						"name": "condition_banner",
						"version": "3.0.2"
					}, {
						"name": "network_banner",
						"version": "3.0.2"
					}
				]
			}
		},
		"version": "3.0.2",
		"lastUpdateDate": new Date(),
		"lastUpdateBy": "beebuddySystemAdmin",
		"createDate": new Date()
	}, {
		"actionType": "login_noconfirm_api_change",
		"chainTemplate": {
			"type": "$loading",
			"dmaType": "open_loading",
			"options": {
				"lock": "true"
			},
			"success": {
				"type": "$set",
				"dmaType": "set_global_to_api",
				"options": {},
				"success": {
					"type": "$network.request",
					"dmaType": "network_request",
					"options": {
						"url": "http://www.dma.beebuddy.net/myfile.json",
						"method": "post",
						"header": {
							"content_type": "json"
						},
						"data": {}
					},
					"success": {
						"type": "$set",
						"dmaType": "response_to_local",
						"options": {
							"result": "{{$jason}}"
						},
						"success": {
							"type": "$loading",
							"dmaType": "hide_loading",
							"options": {
								"lock": "false"
							},
							"success": {
								"type": "$util.condition",
								"dmaType": "check_condition",
								"options": {
									"expression": "true"
								},
								"success": {
									"type": "$global.set",
									"dmaType": "set_global",
									"options": {
										"output1": "{{$get.result.resultData}}"
									},
									"success": {
										"type": "$session.set",
										"dmaType": "set_session",
										"options": {
											"domain": "jasonbase.com",
											"header": {},
											"body": {}
										},
										"success": {
											"type": "$href",
											"dmaType": "change_page",
											"options": {
												"url": "www.mylinkurl.com/myfile.json"
											},
											"success": {},
											"error": {}
										},
										"error": {}
									},
									"error": {}
								},
								"error": {
									"type": "$util.banner",
									"dmaType": "condition_Banner",
									"options": {
										"title": "Hello",
										"description": "{{$get.result.resultMessage}}",
										"type": "error"
									},
									"success": {},
									"error": {}
								}
							}
						}
					},
					"error": {
						"type": "$loading",
						"dmaType": "hide_loading",
						"options": {
							"lock": "false"
						},
						"success": {
							"type": "$util.banner",
							"dmaType": "network_banner",
							"options": {
								"title": "Hello",
								"description": "Can not connect to server.",
								"type": "error"
							},
							"success": {},
							"error": {
								"type": "$util.alert",
								"dmaType": "network_banner",
								"options": {
									"title": "Hello",
									"description": "Do you want to exit?",
									"positiveText": "Yes",
									"negetiveText": "No",
									"form": []
								},
								"success": {},
								"error": {}
							}
						}
					}
				},
				"error": {}
			}
		},
		"refference": {
			"version": "3.0.2",
			"required": {
				"dmaActionChainRequired": [{
						"name": "open_loading",
						"version": "3.0.2"
					}, {
						"name": "set_global_to_api",
						"version": "3.0.2"
					}, {
						"name": "network_request",
						"version": "3.0.2"
					}, {
						"name": "response_to_local",
						"version": "3.0.2"
					}, {
						"name": "hide_loading",
						"version": "3.0.2"
					}, {
						"name": "check_condition",
						"version": "3.0.2"
					}, {
						"name": "set_global",
						"version": "3.0.2"
					}, {
						"name": "set_session",
						"version": "3.0.2"
					}, {
						"name": "change_page",
						"version": "3.0.2"
					}, {
						"name": "condition_banner",
						"version": "3.0.2"
					}, {
						"name": "network_banner",
						"version": "3.0.2"
					}
				]
			}
		},
		"version": "3.0.2",
		"lastUpdateDate": new Date(),
		"lastUpdateBy": "beebuddySystemAdmin",
		"createDate": new Date()
	}, {
		"actionType": "normal_confirm_api_change",
		"chainTemplate": {
			"type": "$util.alert",
			"dmaType": "confirm_dialog",
			"options": {
				"title": "Hello",
				"description": "This is a message",
				"positiveText": "Yes",
				"negetiveText": "No"
			},
			"success": {
				"type": "$loading",
				"dmaType": "open_loading",
				"options": {
					"lock": "true"
				},
				"success": {
					"type": "$set",
					"dmaType": "set_global_to_api",
					"options": {},
					"success": {
						"type": "$network.request",
						"dmaType": "network_request",
						"options": {
							"url": "http://www.dma.beebuddy.net/myfile.json",
							"method": "post",
							"header": {
								"content_type": "json"
							},
							"data": {}
						},
						"success": {
							"type": "$set",
							"dmaType": "response_to_local",
							"options": {
								"result": "{{$jason}}"
							},
							"success": {
								"type": "$loading",
								"dmaType": "hide_loading",
								"options": {
									"lock": "false"
								},
								"success": {
									"type": "$util.condition",
									"dmaType": "check_condition",
									"options": {
										"expression": "true"
									},
									"success": {
										"type": "$global.set",
										"dmaType": "set_global",
										"options": {
											"output1": "{{$get.result.resultData}}"
										},
										"success": {
											"type": "$href",
											"dmaType": "change_page",
											"options": {
												"url": "www.mylinkurl.com/myfile.json"
											},
											"success": {},
											"error": {}
										},
										"error": {}
									},
									"error": {
										"type": "$util.banner",
										"dmaType": "condition_Banner",
										"options": {
											"title": "Hello",
											"description": "{{$get.result.resultMessage}}",
											"type": "error"
										},
										"success": {},
										"error": {}
									}
								}
							}
						},
						"error": {
							"type": "$loading",
							"dmaType": "hide_loading",
							"options": {
								"lock": "false"
							},
							"success": {
								"type": "$util.banner",
								"dmaType": "network_banner",
								"options": {
									"title": "Hello",
									"description": "Can not connect to server.",
									"type": "error"
								},
								"success": {},
								"error": {
									"type": "$util.alert",
									"dmaType": "network_banner",
									"options": {
										"title": "Hello",
										"description": "Do you want to exit?",
										"positiveText": "Yes",
										"negetiveText": "No",
										"form": []
									},
									"success": {},
									"error": {}
								}
							}
						}
					},
					"error": {}
				}
			},
			"error": {}
		},
		"refference": {
			"version": "3.0.2",
			"required": {
				"dmaActionChainRequired": [{
						"name": "confirm_dialog",
						"version": "3.0.2"
					}, {
						"name": "open_loading",
						"version": "3.0.2"
					}, {
						"name": "set_global_to_api",
						"version": "3.0.2"
					}, {
						"name": "network_request",
						"version": "3.0.2"
					}, {
						"name": "response_to_local",
						"version": "3.0.2"
					}, {
						"name": "hide_loading",
						"version": "3.0.2"
					}, {
						"name": "check_condition",
						"version": "3.0.2"
					}, {
						"name": "set_global",
						"version": "3.0.2"
					}, {
						"name": "change_page",
						"version": "3.0.2"
					}, {
						"name": "condition_banner",
						"version": "3.0.2"
					}, {
						"name": "network_banner",
						"version": "3.0.2"
					}
				]
			}
		},
		"version": "3.0.2",
		"lastUpdateDate": new Date(),
		"lastUpdateBy": "beebuddySystemAdmin",
		"createDate": new Date()
	}, {
		"actionType": "normal_confirm_api_self",
		"chainTemplate": {
			"type": "$util.alert",
			"dmaType": "confirm_dialog",
			"options": {
				"title": "Hello",
				"description": "This is a message",
				"positiveText": "Yes",
				"negetiveText": "No"
			},
			"success": {
				"type": "$loading",
				"dmaType": "open_loading",
				"options": {
					"lock": "true"
				},
				"success": {
					"type": "$set",
					"dmaType": "set_global_to_api",
					"options": {},
					"success": {
						"type": "$network.request",
						"dmaType": "network_request",
						"options": {
							"url": "http://www.dma.beebuddy.net/myfile.json",
							"method": "post",
							"header": {
								"content_type": "json"
							},
							"data": {}
						},
						"success": {
							"type": "$set",
							"dmaType": "response_to_local",
							"options": {
								"result": "{{$jason}}"
							},
							"success": {
								"type": "$loading",
								"dmaType": "hide_loading",
								"options": {
									"lock": "false"
								},
								"success": {
									"type": "$util.condition",
									"dmaType": "check_condition",
									"options": {
										"expression": "true"
									},
									"success": {
										"type": "$global.set",
										"dmaType": "set_global",
										"options": {
											"output1": "{{$get.result.resultData}}"
										},
										"success": {},
										"error": {}
									},
									"error": {
										"type": "$util.banner",
										"dmaType": "condition_Banner",
										"options": {
											"title": "Hello",
											"description": "{{$get.result.resultMessage}}",
											"type": "error"
										},
										"success": {},
										"error": {}
									}
								}
							}
						},
						"error": {
							"type": "$loading",
							"dmaType": "hide_loading",
							"options": {
								"lock": "false"
							},
							"success": {
								"type": "$util.banner",
								"dmaType": "network_banner",
								"options": {
									"title": "Hello",
									"description": "Can not connect to server.",
									"type": "error"
								},
								"success": {},
								"error": {
									"type": "$util.alert",
									"dmaType": "network_banner",
									"options": {
										"title": "Hello",
										"description": "Do you want to exit?",
										"positiveText": "Yes",
										"negetiveText": "No",
										"form": []
									},
									"success": {},
									"error": {}
								}
							}
						}
					},
					"error": {}
				}
			},
			"error": {}
		},
		"refference": {
			"version": "3.0.2",
			"required": {
				"dmaActionChainRequired": [{
						"name": "confirm_dialog",
						"version": "3.0.2"
					}, {
						"name": "open_loading",
						"version": "3.0.2"
					}, {
						"name": "set_global_to_api",
						"version": "3.0.2"
					}, {
						"name": "network_request",
						"version": "3.0.2"
					}, {
						"name": "response_to_local",
						"version": "3.0.2"
					}, {
						"name": "hide_loading",
						"version": "3.0.2"
					}, {
						"name": "check_condition",
						"version": "3.0.2"
					}, {
						"name": "set_global",
						"version": "3.0.2"
					}, {
						"name": "condition_banner",
						"version": "3.0.2"
					}, {
						"name": "network_banner",
						"version": "3.0.2"
					}
				]
			}
		},
		"version": "3.0.2",
		"lastUpdateDate": new Date(),
		"lastUpdateBy": "beebuddySystemAdmin",
		"createDate": new Date()
	}, {
		"actionType": "normal_confirm_noapi_change",
		"chainTemplate": {
			"type": "$util.alert",
			"dmaType": "confirm_dialog",
			"options": {
				"title": "Hello",
				"description": "This is a message",
				"positiveText": "Yes",
				"negetiveText": "No"
			},
			"success": {
				"type": "$global.set",
				"dmaType": "set_global",
				"options": {
					"output1": "{{$get.result.resultData}}"
				},
				"success": {
					"type": "$href",
					"dmaType": "change_page",
					"options": {
						"url": "www.mylinkurl.com/myfile.json"
					},
					"success": {},
					"error": {}
				},
				"error": {}
			}
		},
		"refference": {
			"version": "3.0.2",
			"required": {
				"dmaActionChainRequired": [{
						"name": "confirm_dialog",
						"version": "3.0.2"
					}, {
						"name": "set_global",
						"version": "3.0.2"
					}, {
						"name": "change_page",
						"version": "3.0.2"
					}
				]
			}
		},
		"version": "3.0.2",
		"lastUpdateDate": new Date(),
		"lastUpdateBy": "beebuddySystemAdmin",
		"createDate": new Date()
	}, {
		"actionType": "normal_confirm_noapi_self",
		"chainTemplate": {
			"type": "$util.alert",
			"dmaType": "confirm_dialog",
			"options": {
				"title": "Hello",
				"description": "This is a message",
				"positiveText": "Yes",
				"negetiveText": "No"
			},
			"success": {
				"type": "$global.set",
				"dmaType": "set_global",
				"options": {
					"output1": "{{$get.result.resultData}}"
				},
				"success": {},
				"error": {}
			}
		},
		"refference": {
			"version": "3.0.2",
			"required": {
				"dmaActionChainRequired": [{
						"name": "confirm_dialog",
						"version": "3.0.2"
					}, {
						"name": "set_global",
						"version": "3.0.2"
					}
				]
			}
		},
		"version": "3.0.2",
		"lastUpdateDate": new Date(),
		"lastUpdateBy": "beebuddySystemAdmin",
		"createDate": new Date()
	}, {
		"actionType": "normal_noconfirm_api_change",
		"chainTemplate": {
			"type": "$loading",
			"dmaType": "open_loading",
			"options": {
				"lock": "true"
			},
			"success": {
				"type": "$set",
				"dmaType": "set_global_to_api",
				"options": {},
				"success": {
					"type": "$network.request",
					"dmaType": "network_request",
					"options": {
						"url": "http://www.dma.beebuddy.net/myfile.json",
						"method": "post",
						"header": {
							"content_type": "json"
						},
						"data": {}
					},
					"success": {
						"type": "$set",
						"dmaType": "response_to_local",
						"options": {
							"result": "{{$jason}}"
						},
						"success": {
							"type": "$loading",
							"dmaType": "hide_loading",
							"options": {
								"lock": "false"
							},
							"success": {
								"type": "$util.condition",
								"dmaType": "check_condition",
								"options": {
									"expression": "true"
								},
								"success": {
									"type": "$global.set",
									"dmaType": "set_global",
									"options": {
										"output1": "{{$get.result.resultData}}"
									},
									"success": {
										"type": "$href",
										"dmaType": "change_page",
										"options": {
											"url": "www.mylinkurl.com/myfile.json"
										},
										"success": {},
										"error": {}
									},
									"error": {}
								},
								"error": {
									"type": "$util.banner",
									"dmaType": "condition_Banner",
									"options": {
										"title": "Hello",
										"description": "{{$get.result.resultMessage}}",
										"type": "error"
									},
									"success": {},
									"error": {}
								}
							}
						}
					},
					"error": {
						"type": "$loading",
						"dmaType": "hide_loading",
						"options": {
							"lock": "false"
						},
						"success": {
							"type": "$util.banner",
							"dmaType": "network_banner",
							"options": {
								"title": "Hello",
								"description": "Can not connect to server.",
								"type": "error"
							},
							"success": {},
							"error": {
								"type": "$util.alert",
								"dmaType": "network_banner",
								"options": {
									"title": "Hello",
									"description": "Do you want to exit?",
									"positiveText": "Yes",
									"negetiveText": "No",
									"form": []
								},
								"success": {},
								"error": {}
							}
						}
					}
				},
				"error": {}
			}
		},
		"refference": {
			"version": "3.0.2",
			"required": {
				"dmaActionChainRequired": [{
						"name": "open_loading",
						"version": "3.0.2"
					}, {
						"name": "set_global_to_api",
						"version": "3.0.2"
					}, {
						"name": "network_request",
						"version": "3.0.2"
					}, {
						"name": "response_to_local",
						"version": "3.0.2"
					}, {
						"name": "hide_loading",
						"version": "3.0.2"
					}, {
						"name": "check_condition",
						"version": "3.0.2"
					}, {
						"name": "set_global",
						"version": "3.0.2"
					}, {
						"name": "change_page",
						"version": "3.0.2"
					}, {
						"name": "condition_banner",
						"version": "3.0.2"
					}, {
						"name": "network_banner",
						"version": "3.0.2"
					}
				]
			}
		},
		"version": "3.0.2",
		"lastUpdateDate": new Date(),
		"lastUpdateBy": "\"admin\"",
		"createDate": new Date()
	}, {
		"actionType": "normal_noconfirm_api_self",
		"chainTemplate": {
			"type": "$loading",
			"dmaType": "open_loading",
			"options": {
				"lock": "true"
			},
			"success": {
				"type": "$set",
				"dmaType": "set_global_to_api",
				"options": {},
				"success": {
					"type": "$network.request",
					"dmaType": "network_request",
					"options": {
						"url": "http://www.dma.beebuddy.net/myfile.json",
						"method": "post",
						"header": {
							"content_type": "json"
						},
						"data": {}
					},
					"success": {
						"type": "$set",
						"dmaType": "response_to_local",
						"options": {
							"result": "{{$jason}}"
						},
						"success": {
							"type": "$loading",
							"dmaType": "hide_loading",
							"options": {
								"lock": "false"
							},
							"success": {
								"type": "$util.condition",
								"dmaType": "check_condition",
								"options": {
									"expression": "true"
								},
								"success": {
									"type": "$global.set",
									"dmaType": "set_global",
									"options": {
										"output1": "{{$get.result.resultData}}"
									},
									"success": {},
									"error": {}
								},
								"error": {
									"type": "$util.banner",
									"dmaType": "condition_Banner",
									"options": {
										"title": "Hello",
										"description": "{{$get.result.resultMessage}}",
										"type": "error"
									},
									"success": {},
									"error": {}
								}
							}
						}
					},
					"error": {
						"type": "$loading",
						"dmaType": "hide_loading",
						"options": {
							"lock": "false"
						},
						"success": {
							"type": "$util.banner",
							"dmaType": "network_banner",
							"options": {
								"title": "Hello",
								"description": "Can not connect to server.",
								"type": "error"
							},
							"success": {},
							"error": {
								"type": "$util.alert",
								"dmaType": "network_banner",
								"options": {
									"title": "Hello",
									"description": "Do you want to exit?",
									"positiveText": "Yes",
									"negetiveText": "No",
									"form": []
								},
								"success": {},
								"error": {}
							}
						}
					}
				},
				"error": {}
			}
		},
		"refference": {
			"version": "3.0.2",
			"required": {
				"dmaActionChainRequired": [{
						"name": "open_loading",
						"version": "3.0.2"
					}, {
						"name": "set_global_to_api",
						"version": "3.0.2"
					}, {
						"name": "network_request",
						"version": "3.0.2"
					}, {
						"name": "response_to_local",
						"version": "3.0.2"
					}, {
						"name": "hide_loading",
						"version": "3.0.2"
					}, {
						"name": "check_condition",
						"version": "3.0.2"
					}, {
						"name": "set_global",
						"version": "3.0.2"
					}, {
						"name": "condition_banner",
						"version": "3.0.2"
					}, {
						"name": "network_banner",
						"version": "3.0.2"
					}
				]
			}
		},
		"version": "3.0.2",
		"lastUpdateDate": new Date(),
		"lastUpdateBy": "beebuddySystemAdmin",
		"createDate": new Date()
	}, {
		"actionType": "normal_noconfirm_noapi_change",
		"chainTemplate": {
			"type": "$global.set",
			"dmaType": "set_global",
			"options": {
				"output1": "{{$get.result.resultData}}"
			},
			"success": {
				"type": "$href",
				"dmaType": "change_page",
				"options": {
					"url": "www.mylinkurl.com/myfile.json"
				},
				"success": {},
				"error": {}
			},
			"error": {}
		},
		"refference": {
			"version": "3.0.2",
			"required": {
				"dmaActionChainRequired": [{
						"name": "set_global",
						"version": "3.0.2"
					}, {
						"name": "change_page",
						"version": "3.0.2"
					}
				]
			}
		},
		"version": "3.0.2",
		"lastUpdateDate": new Date(),
		"lastUpdateBy": "\"admin\"",
		"createDate": new Date()
	}, {
		"actionType": "normal_noconfirm_noapi_self",
		"chainTemplate": {
			"type": "$global.set",
			"dmaType": "set_global",
			"options": {
				"output1": "{{$get.result.resultData}}"
			},
			"success": {},
			"error": {}
		},
		"refference": {
			"version": "3.0.2",
			"required": {
				"dmaActionChainRequired": [{
						"name": "set_global",
						"version": "3.0.2"
					}
				]
			}
		},
		"version": "3.0.2",
		"lastUpdateDate": new Date(),
		"lastUpdateBy": "\"admin\"",
		"createDate": new Date()
	}, {
		"actionType": "refreshMap_confirm_api_refresh",
		"chainTemplate": {
			"type": "$util.alert",
			"dmaType": "confirm_dialog",
			"options": {
				"title": "Hello",
				"description": "This is a message",
				"positiveText": "Yes",
				"negetiveText": "No"
			},
			"success": {
				"type": "$loading",
				"dmaType": "open_loading",
				"options": {
					"lock": "true"
				},
				"success": {
					"type": "$set",
					"dmaType": "set_global_to_api",
					"options": {},
					"success": {
						"type": "$network.request",
						"dmaType": "network_request",
						"options": {
							"url": "http://www.dma.beebuddy.net/myfile.json",
							"method": "post",
							"header": {
								"content_type": "json"
							},
							"data": {}
						},
						"success": {
							"type": "$set",
							"dmaType": "response_to_local",
							"options": {
								"result": "{{$jason}}"
							},
							"success": {
								"type": "$loading",
								"dmaType": "hide_loading",
								"options": {
									"lock": "false"
								},
								"success": {
									"type": "$util.condition",
									"dmaType": "check_condition",
									"options": {
										"expression": "true"
									},
									"success": {
										"type": "$global.set",
										"dmaType": "set_global",
										"options": {
											"output1": "{{$get.result.resultData}}"
										},
										"success": {
											"type": "$util.property",
											"dmaType": "util_property",
											"options": {
												"name": "",
												"value": "Holy shit!!!",
												"data_source": "data_source",
												"visible": "true",
												"enable": "true"
											},
											"success": {},
											"error": {}
										},
										"error": {}
									},
									"error": {
										"type": "$util.banner",
										"dmaType": "condition_Banner",
										"options": {
											"title": "Hello",
											"description": "{{$get.result.resultMessage}}",
											"type": "error"
										},
										"success": {},
										"error": {}
									}
								}
							}
						},
						"error": {
							"type": "$loading",
							"dmaType": "hide_loading",
							"options": {
								"lock": "false"
							},
							"success": {
								"type": "$util.banner",
								"dmaType": "network_banner",
								"options": {
									"title": "Hello",
									"description": "Can not connect to server.",
									"type": "error"
								},
								"success": {},
								"error": {
									"type": "$util.alert",
									"dmaType": "network_banner",
									"options": {
										"title": "Hello",
										"description": "Do you want to exit?",
										"positiveText": "Yes",
										"negetiveText": "No",
										"form": []
									},
									"success": {},
									"error": {}
								}
							}
						}
					},
					"error": {}
				}
			},
			"error": {}
		},
		"refference": {
			"version": "3.0.2",
			"required": {
				"dmaActionChainRequired": [{
						"name": "confirm_dialog",
						"version": "3.0.2"
					}, {
						"name": "open_loading",
						"version": "3.0.2"
					}, {
						"name": "set_global_to_api",
						"version": "3.0.2"
					}, {
						"name": "network_request",
						"version": "3.0.2"
					}, {
						"name": "response_to_local",
						"version": "3.0.2"
					}, {
						"name": "hide_loading",
						"version": "3.0.2"
					}, {
						"name": "check_condition",
						"version": "3.0.2"
					}, {
						"name": "set_global",
						"version": "3.0.2"
					}, {
						"name": "util_property",
						"version": "3.0.2"
					}, {
						"name": "condition_banner",
						"version": "3.0.2"
					}, {
						"name": "network_banner",
						"version": "3.0.2"
					}
				]
			}
		},
		"version": "3.0.2",
		"lastUpdateDate": new Date(),
		"lastUpdateBy": "beebuddySystemAdmin",
		"createDate": new Date()
	}, {
		"actionType": "refreshMap_noconfirm_api_refresh",
		"chainTemplate": {
			"type": "$loading",
			"dmaType": "open_loading",
			"options": {
				"lock": "true"
			},
			"success": {
				"type": "$set",
				"dmaType": "set_global_to_api",
				"options": {},
				"success": {
					"type": "$network.request",
					"dmaType": "network_request",
					"options": {
						"url": "http://www.dma.beebuddy.net/myfile.json",
						"method": "post",
						"header": {
							"content_type": "json"
						},
						"data": {}
					},
					"success": {
						"type": "$set",
						"dmaType": "response_to_local",
						"options": {
							"result": "{{$jason}}"
						},
						"success": {
							"type": "$loading",
							"dmaType": "hide_loading",
							"options": {
								"lock": "false"
							},
							"success": {
								"type": "$util.condition",
								"dmaType": "check_condition",
								"options": {
									"expression": "true"
								},
								"success": {
									"type": "$global.set",
									"dmaType": "set_global",
									"options": {
										"output1": "{{$get.result.resultData}}"
									},
									"success": {
										"type": "$util.property",
										"dmaType": "util_property",
										"options": {
											"name": "",
											"value": "Holy shit!!!",
											"data_source": "data_source",
											"visible": "true",
											"enable": "true"
										},
										"success": {},
										"error": {}
									},
									"error": {}
								},
								"error": {
									"type": "$util.banner",
									"dmaType": "condition_Banner",
									"options": {
										"title": "Hello",
										"description": "{{$get.result.resultMessage}}",
										"type": "error"
									},
									"success": {},
									"error": {}
								}
							}
						}
					},
					"error": {
						"type": "$loading",
						"dmaType": "hide_loading",
						"options": {
							"lock": "false"
						},
						"success": {
							"type": "$util.banner",
							"dmaType": "network_banner",
							"options": {
								"title": "Hello",
								"description": "Can not connect to server.",
								"type": "error"
							},
							"success": {},
							"error": {
								"type": "$util.alert",
								"dmaType": "network_banner",
								"options": {
									"title": "Hello",
									"description": "Do you want to exit?",
									"positiveText": "Yes",
									"negetiveText": "No",
									"form": []
								},
								"success": {},
								"error": {}
							}
						}
					}
				},
				"error": {}
			}
		},
		"refference": {
			"version": "3.0.2",
			"required": {
				"dmaActionChainRequired": [{
						"name": "open_loading",
						"version": "3.0.2"
					}, {
						"name": "set_global_to_api",
						"version": "3.0.2"
					}, {
						"name": "network_request",
						"version": "3.0.2"
					}, {
						"name": "response_to_local",
						"version": "3.0.2"
					}, {
						"name": "hide_loading",
						"version": "3.0.2"
					}, {
						"name": "check_condition",
						"version": "3.0.2"
					}, {
						"name": "set_global",
						"version": "3.0.2"
					}, {
						"name": "util_property",
						"version": "3.0.2"
					}, {
						"name": "condition_banner",
						"version": "3.0.2"
					}, {
						"name": "network_banner",
						"version": "3.0.2"
					}
				]
			}
		},
		"version": "3.0.2",
		"lastUpdateDate": new Date(),
		"lastUpdateBy": "\"admin\"",
		"createDate": new Date()
	}, {
		"actionType": "logout_confirm_api_change",
		"chainTemplate": {
			"type": "$util.alert",
			"dmaType": "confirm_dialog",
			"options": {
				"title": "Hello",
				"description": "This is a message",
				"positiveText": "Yes",
				"negetiveText": "No"
			},
			"success": {
				"type": "$loading",
				"dmaType": "open_loading",
				"options": {
					"lock": "true"
				},
				"success": {
					"type": "$set",
					"dmaType": "set_global_to_api",
					"options": {},
					"success": {
						"type": "$network.request",
						"dmaType": "network_request",
						"options": {
							"url": "http://www.dma.beebuddy.net/myfile.json",
							"method": "post",
							"header": {
								"content_type": "json"
							},
							"data": {}
						},
						"success": {
							"type": "$set",
							"dmaType": "response_to_local",
							"options": {
								"result": "{{$jason}}"
							},
							"success": {
								"type": "$loading",
								"dmaType": "hide_loading",
								"options": {
									"lock": "false"
								},
								"success": {
									"type": "$util.condition",
									"dmaType": "check_condition",
									"options": {
										"expression": "true"
									},
									"success": {
										"type": "$global.set",
										"dmaType": "set_global",
										"options": {
											"output1": "{{$get.result.resultData}}"
										},
										"success": {
											"type": "$global.resetAll",
											"dmaType": "reset_global",
											"success": {
												"type": "$session.resetAll",
												"dmaType": "reset_session",
												"success": {
													"type": "$passcode.reset",
													"dmaType": "reset_passcode",
													"success": {
														"type": "$once.resetAll",
														"dmaType": "reset_once",
														"success": {
															"type": "$href",
															"dmaType": "change_page",
															"options": {
																"url": "www.mylinkurl.com/myfile.json"
															},
															"success": {},
															"error": {}
														},
														"error": {}
													},
													"error": {}
												},
												"error": {}
											},
											"error": {}
										},
										"error": {}
									},
									"error": {
										"type": "$util.banner",
										"dmaType": "condition_Banner",
										"options": {
											"title": "Hello",
											"description": "{{$get.result.resultMessage}}",
											"type": "error"
										},
										"success": {},
										"error": {}
									}
								}
							}
						},
						"error": {
							"type": "$loading",
							"dmaType": "hide_loading",
							"options": {
								"lock": "false"
							},
							"success": {
								"type": "$util.banner",
								"dmaType": "network_banner",
								"options": {
									"title": "Hello",
									"description": "Can not connect to server.",
									"type": "error"
								},
								"success": {},
								"error": {
									"type": "$util.alert",
									"dmaType": "network_banner",
									"options": {
										"title": "Hello",
										"description": "Do you want to exit?",
										"positiveText": "Yes",
										"negetiveText": "No",
										"form": []
									},
									"success": {},
									"error": {}
								}
							}
						}
					},
					"error": {}
				}
			},
			"error": {}
		},
		"refference": {
			"version": "3.0.2",
			"required": {
				"dmaActionChainRequired": [{
						"name": "confirm_dialog",
						"version": "3.0.2"
					}, {
						"name": "open_loading",
						"version": "3.0.2"
					}, {
						"name": "set_global_to_api",
						"version": "3.0.2"
					}, {
						"name": "network_request",
						"version": "3.0.2"
					}, {
						"name": "response_to_local",
						"version": "3.0.2"
					}, {
						"name": "hide_loading",
						"version": "3.0.2"
					}, {
						"name": "check_condition",
						"version": "3.0.2"
					}, {
						"name": "set_global",
						"version": "3.0.2"
					}, {
						"name": "reset_global",
						"version": "3.0.2"
					}, {
						"name": "reset_session",
						"version": "3.0.2"
					}, {
						"name": "reset_passcode",
						"version": "3.0.2"
					}, {
						"name": "reset_once",
						"version": "3.0.2"
					}, {
						"name": "change_page",
						"version": "3.0.2"
					}, {
						"name": "condition_banner",
						"version": "3.0.2"
					}, {
						"name": "network_banner",
						"version": "3.0.2"
					}
				]
			}
		},
		"version": "3.0.2",
		"lastUpdateDate": new Date(),
		"lastUpdateBy": "beebuddySystemAdmin",
		"createDate": new Date()
	}, {
		"actionType": "logout_confirm_noapi_change",
		"chainTemplate": {
			"type": "$util.alert",
			"dmaType": "confirm_dialog",
			"options": {
				"title": "Hello",
				"description": "This is a message",
				"positiveText": "Yes",
				"negetiveText": "No"
			},
			"success": {
				"type": "$global.set",
				"dmaType": "set_global",
				"options": {
					"output1": "{{$get.result.resultData}}"
				},
				"success": {
					"type": "$global.resetAll",
					"dmaType": "reset_global",
					"success": {
						"type": "$session.resetAll",
						"dmaType": "reset_session",
						"success": {
							"type": "$passcode.reset",
							"dmaType": "reset_passcode",
							"success": {
								"type": "$once.resetAll",
								"dmaType": "reset_once",
								"success": {
									"type": "$href",
									"dmaType": "change_page",
									"options": {
										"url": "www.mylinkurl.com/myfile.json"
									},
									"success": {},
									"error": {}
								},
								"error": {}
							},
							"error": {}
						},
						"error": {}
					},
					"error": {}
				},
				"error": {}
			}
		},
		"refference": {
			"version": "3.0.2",
			"required": {
				"dmaActionChainRequired": [{
						"name": "confirm_dialog",
						"version": "3.0.2"
					}, {
						"name": "set_global",
						"version": "3.0.2"
					}, {
						"name": "reset_global",
						"version": "3.0.2"
					}, {
						"name": "reset_session",
						"version": "3.0.2"
					}, {
						"name": "reset_passcode",
						"version": "3.0.2"
					}, {
						"name": "reset_once",
						"version": "3.0.2"
					}, {
						"name": "change_page",
						"version": "3.0.2"
					}
				]
			}
		},
		"version": "3.0.2",
		"lastUpdateDate": new Date(),
		"lastUpdateBy": "beebuddySystemAdmin",
		"createDate": new Date()
	}, {
		"actionType": "logout_noconfirm_api_change",
		"chainTemplate": {
			"type": "$loading",
			"dmaType": "open_loading",
			"options": {
				"lock": "true"
			},
			"success": {
				"type": "$set",
				"dmaType": "set_global_to_api",
				"options": {},
				"success": {
					"type": "$network.request",
					"dmaType": "network_request",
					"options": {
						"url": "http://www.dma.beebuddy.net/myfile.json",
						"method": "post",
						"header": {
							"content_type": "json"
						},
						"data": {}
					},
					"success": {
						"type": "$set",
						"dmaType": "response_to_local",
						"options": {
							"result": "{{$jason}}"
						},
						"success": {
							"type": "$loading",
							"dmaType": "hide_loading",
							"options": {
								"lock": "false"
							},
							"success": {
								"type": "$util.condition",
								"dmaType": "check_condition",
								"options": {
									"expression": "true"
								},
								"success": {
									"type": "$global.set",
									"dmaType": "set_global",
									"options": {
										"output1": "{{$get.result.resultData}}"
									},
									"success": {
										"type": "$global.resetAll",
										"dmaType": "reset_global",
										"success": {
											"type": "$session.resetAll",
											"dmaType": "reset_session",
											"success": {
												"type": "$passcode.reset",
												"dmaType": "reset_passcode",
												"success": {
													"type": "$once.resetAll",
													"dmaType": "reset_once",
													"success": {
														"type": "$href",
														"dmaType": "change_page",
														"options": {
															"url": "www.mylinkurl.com/myfile.json"
														},
														"success": {},
														"error": {}
													},
													"error": {}
												},
												"error": {}
											},
											"error": {}
										},
										"error": {}
									},
									"error": {}
								},
								"error": {
									"type": "$util.banner",
									"dmaType": "condition_Banner",
									"options": {
										"title": "Hello",
										"description": "{{$get.result.resultMessage}}",
										"type": "error"
									},
									"success": {},
									"error": {}
								}
							}
						}
					},
					"error": {
						"type": "$loading",
						"dmaType": "hide_loading",
						"options": {
							"lock": "false"
						},
						"success": {
							"type": "$util.banner",
							"dmaType": "network_banner",
							"options": {
								"title": "Hello",
								"description": "Can not connect to server.",
								"type": "error"
							},
							"success": {},
							"error": {
								"type": "$util.alert",
								"dmaType": "network_banner",
								"options": {
									"title": "Hello",
									"description": "Do you want to exit?",
									"positiveText": "Yes",
									"negetiveText": "No",
									"form": []
								},
								"success": {},
								"error": {}
							}
						}
					}
				},
				"error": {}
			}
		},
		"refference": {
			"version": "3.0.2",
			"required": {
				"dmaActionChainRequired": [{
						"name": "open_loading",
						"version": "3.0.2"
					}, {
						"name": "set_global_to_api",
						"version": "3.0.2"
					}, {
						"name": "network_request",
						"version": "3.0.2"
					}, {
						"name": "response_to_local",
						"version": "3.0.2"
					}, {
						"name": "hide_loading",
						"version": "3.0.2"
					}, {
						"name": "check_condition",
						"version": "3.0.2"
					}, {
						"name": "set_global",
						"version": "3.0.2"
					}, {
						"name": "reset_global",
						"version": "3.0.2"
					}, {
						"name": "reset_session",
						"version": "3.0.2"
					}, {
						"name": "reset_passcode",
						"version": "3.0.2"
					}, {
						"name": "reset_once",
						"version": "3.0.2"
					}, {
						"name": "change_page",
						"version": "3.0.2"
					}, {
						"name": "condition_banner",
						"version": "3.0.2"
					}, {
						"name": "network_banner",
						"version": "3.0.2"
					}
				]
			}
		},
		"version": "3.0.2",
		"lastUpdateDate": new Date(),
		"lastUpdateBy": "beebuddySystemAdmin",
		"createDate": new Date()
	}, {
		"actionType": "logout_noconfirm_noapi_change",
		"chainTemplate": {
			"type": "$global.set",
			"dmaType": "set_global",
			"options": {
				"output1": "{{$get.result.resultData}}"
			},
			"success": {
				"type": "$global.resetAll",
				"dmaType": "reset_global",
				"success": {
					"type": "$session.resetAll",
					"dmaType": "reset_session",
					"success": {
						"type": "$passcode.reset",
						"dmaType": "reset_passcode",
						"success": {
							"type": "$once.resetAll",
							"dmaType": "reset_once",
							"success": {
								"type": "$href",
								"dmaType": "change_page",
								"options": {
									"url": "www.mylinkurl.com/myfile.json"
								},
								"success": {},
								"error": {}
							},
							"error": {}
						},
						"error": {}
					},
					"error": {}
				},
				"error": {}
			},
			"error": {}
		},
		"refference": {
			"version": "3.0.2",
			"required": {
				"dmaActionChainRequired": [{
						"name": "set_global",
						"version": "3.0.2"
					}, {
						"name": "reset_global",
						"version": "3.0.2"
					}, {
						"name": "reset_session",
						"version": "3.0.2"
					}, {
						"name": "reset_passcode",
						"version": "3.0.2"
					}, {
						"name": "reset_once",
						"version": "3.0.2"
					}, {
						"name": "change_page",
						"version": "3.0.2"
					}
				]
			}
		},
		"version": "3.0.2",
		"lastUpdateDate": new Date(),
		"lastUpdateBy": "beebuddySystemAdmin",
		"createDate": new Date()
	}, {
		"actionType": "loginPin_confirm_api_change",
		"chainTemplate": {
			"type": "$util.alert",
			"dmaType": "confirm_dialog",
			"options": {
				"title": "Hello",
				"description": "This is a message",
				"positiveText": "Yes",
				"negetiveText": "No"
			},
			"success": {
				"type": "$loading",
				"dmaType": "open_loading",
				"options": {
					"lock": "true"
				},
				"success": {
					"type": "$set",
					"dmaType": "set_global_to_api",
					"options": {},
					"success": {
						"type": "$network.request",
						"dmaType": "network_request",
						"options": {
							"url": "http://www.dma.beebuddy.net/myfile.json",
							"method": "post",
							"header": {
								"content_type": "json"
							},
							"data": {}
						},
						"success": {
							"type": "$set",
							"dmaType": "response_to_local",
							"options": {
								"result": "{{$jason}}"
							},
							"success": {
								"type": "$loading",
								"dmaType": "hide_loading",
								"options": {
									"lock": "false"
								},
								"success": {
									"type": "$util.condition",
									"dmaType": "check_condition",
									"options": {
										"expression": "true"
									},
									"success": {
										"type": "$global.set",
										"dmaType": "set_global",
										"options": {
											"output1": "{{$get.result.resultData}}"
										},
										"success": {
											"type": "$session.set",
											"dmaType": "set_session",
											"options": {
												"domain": "jasonbase.com",
												"header": {},
												"body": {}
											},
											"success": {
												"type": "$passcode.create",
												"dmaType": "passcode_create",
												"options": {},
												"success": {
													"type": "$href",
													"dmaType": "change_page",
													"options": {
														"url": "www.mylinkurl.com/myfile.json"
													},
													"success": {},
													"error": {}
												},
												"error": {
													"type": "$util.banner",
													"dmaType": "network_banner",
													"options": {
														"title": "Result",
														"description": "Error",
														"type": "error"
													},
													"success": {},
													"error": {}
												}
											},
											"error": {
												"type": "$util.banner",
												"dmaType": "network_banner",
												"options": {
													"title": "Result",
													"description": "Error",
													"type": "error"
												},
												"success": {},
												"error": {}
											}
										},
										"error": {}
									},
									"error": {
										"type": "$util.banner",
										"dmaType": "condition_Banner",
										"options": {
											"title": "Hello",
											"description": "{{$get.result.resultMessage}}",
											"type": "error"
										},
										"success": {},
										"error": {}
									}
								}
							}
						},
						"error": {
							"type": "$loading",
							"dmaType": "hide_loading",
							"options": {
								"lock": "false"
							},
							"success": {
								"type": "$util.banner",
								"dmaType": "network_banner",
								"options": {
									"title": "Hello",
									"description": "Can not connect to server.",
									"type": "error"
								},
								"success": {},
								"error": {
									"type": "$util.alert",
									"dmaType": "network_banner",
									"options": {
										"title": "Hello",
										"description": "Do you want to exit?",
										"positiveText": "Yes",
										"negetiveText": "No",
										"form": []
									},
									"success": {},
									"error": {}
								}
							}
						}
					},
					"error": {}
				}
			},
			"error": {}
		},
		"refference": {
			"version": "3.0.3",
			"required": {
				"dmaActionChainRequired": [{
						"name": "confirm_dialog",
						"version": "3.0.2"
					}, {
						"name": "open_loading",
						"version": "3.0.2"
					}, {
						"name": "set_global_to_api",
						"version": "3.0.2"
					}, {
						"name": "network_request",
						"version": "3.0.2"
					}, {
						"name": "response_to_local",
						"version": "3.0.2"
					}, {
						"name": "hide_loading",
						"version": "3.0.2"
					}, {
						"name": "check_condition",
						"version": "3.0.2"
					}, {
						"name": "set_global",
						"version": "3.0.2"
					}, {
						"name": "set_session",
						"version": "3.0.2"
					}, {
						"name": "passcode_create",
						"version": "3.0.2"
					}, {
						"name": "change_page",
						"version": "3.0.2"
					}, {
						"name": "network_banner",
						"version": "3.0.2"
					}, {
						"name": "condition_banner",
						"version": "3.0.2"
					}
				]
			}
		},
		"version": "3.0.3",
		"lastUpdateDate": new Date(),
		"lastUpdateBy": "beebuddySystemAdmin",
		"createDate": new Date()
	}, {
		"actionType": "loginPin_noconfirm_api_change",
		"chainTemplate": {
			"type": "$loading",
			"dmaType": "open_loading",
			"options": {
				"lock": "true"
			},
			"success": {
				"type": "$set",
				"dmaType": "set_global_to_api",
				"options": {},
				"success": {
					"type": "$network.request",
					"dmaType": "network_request",
					"options": {
						"url": "http://www.dma.beebuddy.net/myfile.json",
						"method": "post",
						"header": {
							"content_type": "json"
						},
						"data": {}
					},
					"success": {
						"type": "$set",
						"dmaType": "response_to_local",
						"options": {
							"result": "{{$jason}}"
						},
						"success": {
							"type": "$loading",
							"dmaType": "hide_loading",
							"options": {
								"lock": "false"
							},
							"success": {
								"type": "$util.condition",
								"dmaType": "check_condition",
								"options": {
									"expression": "true"
								},
								"success": {
									"type": "$global.set",
									"dmaType": "set_global",
									"options": {
										"output1": "{{$get.result.resultData}}"
									},
									"success": {
										"type": "$session.set",
										"dmaType": "set_session",
										"options": {
											"domain": "jasonbase.com",
											"header": {},
											"body": {}
										},
										"success": {
											"type": "$passcode.create",
											"dmaType": "passcode_create",
											"options": {},
											"success": {
												"type": "$href",
												"dmaType": "change_page",
												"options": {
													"url": "www.mylinkurl.com/myfile.json"
												},
												"success": {},
												"error": {}
											},
											"error": {
												"type": "$util.banner",
												"dmaType": "network_banner",
												"options": {
													"title": "Result",
													"description": "Error",
													"type": "error"
												},
												"success": {},
												"error": {}
											}
										},
										"error": {
											"type": "$util.banner",
											"dmaType": "network_banner",
											"options": {
												"title": "Result",
												"description": "Error",
												"type": "error"
											},
											"success": {},
											"error": {}
										}
									},
									"error": {}
								},
								"error": {
									"type": "$util.banner",
									"dmaType": "condition_Banner",
									"options": {
										"title": "Hello",
										"description": "{{$get.result.resultMessage}}",
										"type": "error"
									},
									"success": {},
									"error": {}
								}
							}
						}
					},
					"error": {
						"type": "$loading",
						"dmaType": "hide_loading",
						"options": {
							"lock": "false"
						},
						"success": {
							"type": "$util.banner",
							"dmaType": "network_banner",
							"options": {
								"title": "Hello",
								"description": "Can not connect to server.",
								"type": "error"
							},
							"success": {},
							"error": {
								"type": "$util.alert",
								"dmaType": "network_banner",
								"options": {
									"title": "Hello",
									"description": "Do you want to exit?",
									"positiveText": "Yes",
									"negetiveText": "No",
									"form": []
								},
								"success": {},
								"error": {}
							}
						}
					}
				},
				"error": {}
			}
		},
		"refference": {
			"version": "3.0.3",
			"required": {
				"dmaActionChainRequired": [{
						"name": "open_loading",
						"version": "3.0.2"
					}, {
						"name": "set_global_to_api",
						"version": "3.0.2"
					}, {
						"name": "network_request",
						"version": "3.0.2"
					}, {
						"name": "response_to_local",
						"version": "3.0.2"
					}, {
						"name": "hide_loading",
						"version": "3.0.2"
					}, {
						"name": "check_condition",
						"version": "3.0.2"
					}, {
						"name": "set_global",
						"version": "3.0.2"
					}, {
						"name": "set_session",
						"version": "3.0.2"
					}, {
						"name": "passcode_create",
						"version": "3.0.2"
					}, {
						"name": "change_page",
						"version": "3.0.2"
					}, {
						"name": "network_banner",
						"version": "3.0.2"
					}, {
						"name": "condition_banner",
						"version": "3.0.2"
					}
				]
			}
		},
		"version": "3.0.3",
		"lastUpdateDate": new Date(),
		"lastUpdateBy": "\"admin\"",
		"createDate": new Date()
	}, {
		"actionType": "normal_noconfirm_api_initialScreen",
		"chainTemplate": {
			"type": "$loading",
			"dmaType": "open_loading",
			"options": {
				"lock": "true"
			},
			"success": {
				"type": "$set",
				"dmaType": "set_global_to_api",
				"options": {},
				"success": {
					"type": "$network.request",
					"dmaType": "network_request",
					"options": {
						"url": "http://www.dma.beebuddy.net/myfile.json",
						"method": "post",
						"header": {
							"content_type": "json"
						},
						"data": {}
					},
					"success": {
						"type": "$set",
						"dmaType": "response_to_local",
						"options": {
							"result": "{{$jason}}"
						},
						"success": {
							"type": "$loading",
							"dmaType": "hide_loading",
							"options": {
								"lock": "false"
							},
							"success": {
								"type": "$util.condition",
								"dmaType": "check_condition",
								"options": {
									"expression": "true"
								},
								"success": {
									"type": "$set",
									"dmaType": "set_mainform",
									"options": {},
									"success": {
										"type": "$render",
										"dmaType": "render",
										"success": {},
										"error": {}
									},
									"error": {}
								},
								"error": {
									"type": "$util.banner",
									"dmaType": "condition_Banner",
									"options": {
										"title": "Hello",
										"description": "{{$get.result.resultMessage}}",
										"type": "error"
									},
									"success": {},
									"error": {}
								}
							}
						}
					},
					"error": {
						"type": "$loading",
						"dmaType": "hide_loading",
						"options": {
							"lock": "false"
						},
						"success": {
							"type": "$util.banner",
							"dmaType": "network_banner",
							"options": {
								"title": "Hello",
								"description": "Can not connect to server.",
								"type": "error"
							},
							"success": {},
							"error": {
								"type": "$util.alert",
								"dmaType": "network_banner",
								"options": {
									"title": "Hello",
									"description": "Do you want to exit?",
									"positiveText": "Yes",
									"negetiveText": "No",
									"form": []
								},
								"success": {},
								"error": {}
							}
						}
					}
				},
				"error": {}
			}
		},
		"refference": {
			"version": "3.0.4",
			"required": {
				"dmaActionChainRequired": [{
						"name": "open_loading",
						"version": "3.0.2"
					}, {
						"name": "set_global_to_api",
						"version": "3.0.2"
					}, {
						"name": "network_request",
						"version": "3.0.2"
					}, {
						"name": "response_to_local",
						"version": "3.0.2"
					}, {
						"name": "hide_loading",
						"version": "3.0.2"
					}, {
						"name": "check_condition",
						"version": "3.0.2"
					}, {
						"name": "set_mainform",
						"version": "3.0.3"
					}, {
						"name": "render",
						"version": "3.0.3"
					}, {
						"name": "condition_banner",
						"version": "3.0.2"
					}, {
						"name": "network_banner",
						"version": "3.0.2"
					}
				]
			}
		},
		"version": "3.0.4",
		"createDate": new Date(),
		"lastUpdateDate": new Date(),
		"createBy": "systemadmin",
		"lastUpdateBy": "\"admin\""
	}, {
		"actionType": "autoLoginPin_noconfirm_api_change",
		"chainTemplate": {
			"type": "$loading",
			"dmaType": "open_loading",
			"options": {
				"lock": "true"
			},
			"success": {
				"type": "$set",
				"dmaType": "set_global_to_api",
				"options": {},
				"success": {
					"type": "$network.request",
					"dmaType": "network_request",
					"options": {
						"url": "http://www.dma.beebuddy.net/myfile.json",
						"method": "post",
						"header": {
							"content_type": "json"
						},
						"data": {}
					},
					"success": {
						"type": "$set",
						"dmaType": "response_to_local",
						"options": {
							"result": "{{$jason}}"
						},
						"success": {
							"type": "$loading",
							"dmaType": "hide_loading",
							"options": {
								"lock": "false"
							},
							"success": {
								"type": "$util.condition",
								"dmaType": "check_condition",
								"options": {
									"expression": "true"
								},
								"success": {
									"type": "$global.set",
									"dmaType": "set_global",
									"options": {
										"output1": "{{$get.result.resultData}}"
									},
									"success": {
										"type": "$href",
										"dmaType": "change_page",
										"options": {
											"url": "www.mylinkurl.com/myfile.json"
										},
										"success": {},
										"error": {}
									},
									"error": {}
								},
								"error": {
									"type": "$util.banner",
									"dmaType": "condition_Banner",
									"options": {
										"title": "Hello",
										"description": "{{$get.result.resultMessage}}",
										"type": "error"
									},
									"success": {},
									"error": {}
								}
							}
						}
					},
					"error": {
						"type": "$loading",
						"dmaType": "hide_loading",
						"options": {
							"lock": "false"
						},
						"success": {
							"type": "$util.banner",
							"dmaType": "network_banner",
							"options": {
								"title": "Hello",
								"description": "Can not connect to server.",
								"type": "error"
							},
							"success": {},
							"error": {
								"type": "$util.alert",
								"dmaType": "network_banner",
								"options": {
									"title": "Hello",
									"description": "Do you want to exit?",
									"positiveText": "Yes",
									"negetiveText": "No",
									"form": []
								},
								"success": {},
								"error": {}
							}
						}
					}
				},
				"error": {}
			}
		},
		"refference": {
			"version": "3.0.2",
			"required": {
				"dmaActionChainRequired": [{
						"name": "open_loading",
						"version": "3.0.2"
					}, {
						"name": "set_global_to_api",
						"version": "3.0.2"
					}, {
						"name": "network_request",
						"version": "3.0.2"
					}, {
						"name": "response_to_local",
						"version": "3.0.2"
					}, {
						"name": "hide_loading",
						"version": "3.0.2"
					}, {
						"name": "check_condition",
						"version": "3.0.2"
					}, {
						"name": "set_global",
						"version": "3.0.2"
					}, {
						"name": "change_page",
						"version": "3.0.2"
					}, {
						"name": "condition_banner",
						"version": "3.0.2"
					}, {
						"name": "network_banner",
						"version": "3.0.2"
					}
				]
			}
		},
		"version": "3.0.2",
		"createDate": new Date(),
		"lastUpdateDate": new Date(),
		"createBy": "beebuddySystemAdmin",
		"lastUpdateBy": "beebuddySystemAdmin"
	}, {
		"actionType": "autoLoginPin_noconfirm_noapi_change",
		"chainTemplate": {
			"type": "$global.set",
			"dmaType": "set_global",
			"options": {
				"output1": "{{$get.result.resultData}}"
			},
			"success": {
				"type": "$href",
				"dmaType": "change_page",
				"options": {
					"url": "www.mylinkurl.com/myfile.json"
				},
				"success": {},
				"error": {}
			},
			"error": {}
		},
		"refference": {
			"version": "3.0.2",
			"required": {
				"dmaActionChainRequired": [{
						"name": "set_global",
						"version": "3.0.2"
					}, {
						"name": "change_page",
						"version": "3.0.2"
					}
				]
			}
		},
		"version": "3.0.2",
		"createDate": new Date(),
		"lastUpdateDate": new Date(),
		"createBy": "beebuddySystemAdmin",
		"lastUpdateBy": "beebuddySystemAdmin"
	}
])

db.response_status_type.drop()
db.createCollection("response_status_type")
db.response_status_type.insert(
	[
		{
			"labelName" : "Internal process of ServiceGateway",
			"value" : "200"
		},
		{
			"labelName" : "Field validation on mobile",
			"value" : "201"
		},
		{
			"labelName" : "Authentication error when calling an external API",
			"value" : "202"
		},
		{
			"labelName" : "HttpClient error when calling an external API",
			"value" : "203"
		},
		{
			"labelName" : "Result from the external API processing",
			"value" : "204"
		},
		{
			"labelName" : "Internal process of DMA",
			"value" : "300"
		}
	]
)
